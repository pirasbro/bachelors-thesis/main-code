module "docker-swarm" {
  source  = "trajano/swarm-aws/docker"
  version = "~>5.0"

  name                     = var.name
  vpc_id                   = aws_vpc.main.id
  managers                 = var.managers
  workers                  = var.workers
  instance_type            = var.instance_type
  ssh_authorization_method = "iam"

  additional_security_group_ids = [
    aws_security_group.exposed.id,
  ]
}